# README #

### What is this repository for? ###

* Backend side of Todo-Tracker app

### How do I get set up? ###

Clone repo and run
 - `npm install`

To run in development mode:
 - `npm run dev`

To run in staging mode:
 - `npm run start`

#### Live server:
https://rokk3r-todo.herokuapp.com/api/v1/

#### Swagger:
https://rokk3r-todo.herokuapp.com/api/v1/docs/#/