import objectId from "joi-objectid";
import Joi from "joi";

Joi.objectId = objectId(Joi);

export default {
  readAll: {
    query: {
      offset: Joi.number().integer(),
      limit: Joi.number().integer()
    }
  },

  create: {
    body: {
      name: Joi.string().required(),
      dueDate: Joi.string()
        .isoDate()
        .required(),
      priority: Joi.number()
        .min(1)
        .max(5)
        .required()
    }
  },

  update: {
    body: {
      name: Joi.string().required(),
      dueDate: Joi.string()
        .isoDate()
        .required(),
      priority: Joi.number()
        .min(1)
        .max(5)
        .required()
    }
  },

  delete: {
    path: {
      id: Joi.objectId().required()
    }
  }
};
