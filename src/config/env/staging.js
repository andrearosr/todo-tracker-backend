import path from "path";

export const dbConfig = {
  db: "mongodb://root:root@ds221228.mlab.com:21228/rokk3r-todo"
};

export const appConfig = {
  env: "staging",
  host: "https://rokk3r-todo.herokuapp.com",
  path: "/v1",
  basePath: "/api",
  port: process.env.PORT,
  publicPort: process.env.PORT,
  root: path.join(__dirname, "../../../")
};

export const constants = {
  pagination: {
    limit: 40,
    offset: 0
  }
};
