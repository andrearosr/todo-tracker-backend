import { Router } from "express";

import task from "./task";

const router = new Router();
router.use(task);

export default router;
