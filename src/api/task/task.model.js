import mongoose from "mongoose";
import validate from "mongoose-validator";
import paginate from "mongoose-paginate";
import uniqueValidator from "mongoose-unique-validator";
import crypto from "crypto";

// import { average } from '../helpers/utils';
import config from "../../config/env";

const Schema = mongoose.Schema;

/**
 * @swagger
 * definition:
 *   listOfTasks:
 *     type: array
 *     items:
 *       $ref: '#/definitions/Task'
 *   Task:
 *     type: object
 *     properties:
 *       name:
 *         type: string
 *       dueDate:
 *         type: string
 *         format: date-time
 *       priority:
 *         type: integer
 *     required:
 *       - name
 *       - dueDate
 *       - priority
 */

const TaskSchema = new Schema(
  {
    name: {
      type: String,
      required: "This is a required field"
    },
    dueDate: {
      type: Date,
      required: "This is a required field"
    },
    priority: {
      type: Number,
      min: 1,
      max: 5,
      required: "This is a required field"
    }
  },
  {
    timestamps: true,
    toObject: { virtuals: true },
    toJSON: { virtuals: true }
  }
);

TaskSchema.index({ name: "text" });

TaskSchema.virtual("isOverdue").get(function() {
  const today = new Date();
  const dueDate = this.dueDate;

  return today > dueDate;
});

TaskSchema.plugin(uniqueValidator);
TaskSchema.plugin(paginate);

export default mongoose.model("Task", TaskSchema);
