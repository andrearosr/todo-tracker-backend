import express from "express";
import validate from "express-validation";

import Task from "./task.controller";
import taskValidator from "../../services/validations/task";
import { catchErrors } from "../../helpers/errors";

const router = express.Router(); // eslint-disable-line new-cap

validate.options({
  allowUnknownBody: false
});

router
  .route("/tasks")
  .get(validate(taskValidator.readAll), catchErrors(Task.readAll))
  .post(validate(taskValidator.create), catchErrors(Task.create))
  .put(validate(taskValidator.update), catchErrors(Task.update));

router
  .route("/task/:id")
  .delete(validate(taskValidator.delete), catchErrors(Task.delete));

export default router;
