import status from "http-status";
import axios from "axios";

import Task from "./task.model";
import config from "../../config/env";
import { APIError } from "../../helpers/errors";
import { paginate } from "../../helpers/utils";

const UserController = {
  /**
   * @swagger
   * /tasks:
   *   get:
   *     tags:
   *      - Task
   *     description: Show all tasks
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: limit
   *         description: pagination limit.
   *         in: query
   *         required: false
   *         type: string
   *       - name: offset
   *         description: pagination offset.
   *         in: query
   *         required: false
   *         type: string
   *     responses:
   *       200:
   *         description: return an array of users'
   */

  async readAll(req, res) {
    const offset = paginate.offset(req.query.offset);
    const limit = paginate.limit(req.query.limit);

    const find = req.query.find || {};
    const sort = req.query.sort || {
      createdAt: 1
    };

    const tasks = await Task.paginate(find, {
      sort,
      offset,
      limit
    });
    res.json(tasks);
  },

  /**
   * @swagger
   * /tasks:
   *   post:
   *     tags:
   *      - Task
   *     description: Create tasks
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: task
   *         description: task object.
   *         in: body
   *         required: true
   *         schema:
   *           $ref: '#/definitions/Task'
   *     responses:
   *       200:
   *         description: Successfully created
   *         schema:
   *           allOf:
   *              - $ref: '#/definitions/Task'
   *              - properties:
   *                  id:
   *                    type: string
   *                  createdAt:
   *                    type: string
   *                    format: date-time
   *                  updatedAt:
   *                    type: string
   *                    format: date-time
   */
  async create(req, res) {
    const newTask = await Task.create(req.body);
    res.json({ task: newTask });
  },

  /**
   * @swagger
   * /tasks:
   *   patch:
   *     tags:
   *      - Task
   *     description: updates a task
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: task
   *         description: task object.
   *         in: body
   *         required: true
   *         schema:
   *           $ref: '#/definitions/Task'
   *     responses:
   *       200:
   *         description: User object'
   */

  async update(req, res) {
    const task = req.task;
    const updatedTask = await task.save();
    res.status(status.OK).json(updatedTask);
  },

  /**
   * @swagger
   * /tasks/{id}:
   *   delete:
   *     tags:
   *      - Task
   *     description: delete a task
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: id
   *         description: exchange id.
   *         in: path
   *         required: true
   *         type: string
   *     responses:
   *       204:
   *         description: Ok'
   */

  async delete(req, res) {
    const task = await Task.findById(req.params.id);
    if (!task) throw new APIError("Task not found", status.NOT_FOUND);
    await task.remove();
    res.status(status.NO_CONTENT).end();
  }
};

export default UserController;
